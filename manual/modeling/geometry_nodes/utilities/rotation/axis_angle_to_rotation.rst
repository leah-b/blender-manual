.. index:: Geometry Nodes; Axis Angle to Rotation
.. _bpy.types.FunctionNodeAxisAngleToRotation:

***************************
Axis Angle to Rotation Node
***************************

.. figure:: /images/node-types_FunctionNodeAxisAngleToRotation.png
    :align: right
    :alt: Axis Angle to Rotation node.

The *Axis Angle to Rotation* node converts a :ref:`axis angle <axis angle mode>` rotation to a standard rotation
value.

Inputs
======

Axis
    Unit vector representing the axis to rotate around.

Angle
    The rotation angle around the axis.


Outputs
=======

Rotation
    Standard rotation value.
