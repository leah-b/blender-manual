.. index:: Geometry Nodes; Rotation to Quaternion
.. _bpy.types.FunctionNodeRotationToQuaternion:

***************************
Rotation to Quaternion Node
***************************

.. figure:: /images/node-types_FunctionNodeRotationToQuaternion.png
   :align: right
   :alt: Rotation to Quaternion node.

The *Rotation to Quaternion* node converts a standard rotation socket value to a
:ref:`quaternion rotation <quaternion mode>` rotation.

Inputs
======

Rotation
    Standard rotation socket value.


Outputs
=======

W
    The W value of the quaternion.
X
    The X value of the quaternion.
Y
    The Y value of the quaternion.
Z
    The Z value of the quaternion.
